
def build(setup_kwargs):
    from zig_wasmer import build
    from zig_wasmer.examples import module
    build.modules(setup_kwargs, zig_files=[
        {
            'src': 'zig_wasmer/examples/fib_wasi.zig',
            'target': 'wasm32-wasi',
        },
        {
            'src': 'zig_wasmer/examples/fib_wasm.zig',
            'mode': 'Debug',
        },
        module.Example.to_dict(),
    ])
