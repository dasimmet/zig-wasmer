<!---
sphinx-quickstart on Sun Mar  5 20:21:55 2023.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.
-->

[Source Code on Giltab](repo://)  

```{toctree}
:hidden:

API Documentation <apidoc/modules>
cli
```

```{include} ../README.md
```

Above is the included README.  
Start writing your docs here.  
Here is an example reference to the main function:  
[](#zig_wasmer.cli.main)

# Indices and tables

[Genindex](genindex)  
[API Documentation](apidoc/modules)  
[Coverage](relative://htmlcov/index)  
