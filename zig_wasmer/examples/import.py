
from zig_wasmer.examples.module import Example

# This shows how another class might use an imported WASM module

if __name__ == '__main__':
    Example.initialize()
    Example.exports.main()
