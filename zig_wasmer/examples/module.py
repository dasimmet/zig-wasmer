from zig_wasmer.module import WasiModule


class Example(WasiModule):
    decorate = {
        'Member': WasiModule.decorators.Int(size=64),
        'formatptr': WasiModule.decorators.DynamicByteArray('formatlen'),
    }


if __name__ == '__main__':
    Example.initialize()

    print("Dumping Wasm Environment:")
    print("-------------------------")
    from zig_wasmer.wasmer import WasmEnvToJson
    import json
    print(json.dumps(Example.context.instance.exports, indent=4, default=WasmEnvToJson))
    print("-------------------------")

    print("Reading wasm member from python:", Example.exports.Member)
    print("Reading exported string in wasm memory: ", bytes(Example.exports.formatptr))
    print("Writing Bytes Into wasm memory: ")
    Example.exports.formatptr = "Hello, World from WASM\n".encode()
    print("Reading exported string in wasm memory: ", bytes(Example.exports.formatptr))

    print("Setting wasm member to:", 4242)
    Example.exports.Member = 4242

    print("Calling main function:")
    print("----------------------")
    Example.exports.main()
