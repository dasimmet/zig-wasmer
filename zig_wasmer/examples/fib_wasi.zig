
export fn fib(n: u64) u64 {
    return @import("fib.zig").fib(n);
}