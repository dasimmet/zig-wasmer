const std = @import("std");

pub fn fib(n: u64) u64 {
    switch (n) {
        0 => {return 0;},
        else => {
            var i: u64 =  3;
            var t1: u64 = 0;
            var t2: u64 = 1;
            var  nextTerm = t1 + t2;

            while (i <= n) : ({i += 1;}) {
                
                t1 = t2;
                t2 = nextTerm;
                
                nextTerm = std.math.add(u64, t1,t2) catch {
                    @panic("Fib I64 Overflow\n");
                };
            }

            return nextTerm;
        }
    }
}
