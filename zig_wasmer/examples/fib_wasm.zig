const std = @import("std");

export fn fib(n: u64) u64 {
    return @import("fib.zig").fib(n);
}

pub fn panic(msg: []const u8, trace: ?*std.builtin.StackTrace, errno: ?usize) noreturn {
    // _ = errno;
    _ = trace;
    _ = errno;
    _ = wasm_panic(@ptrToInt(msg.ptr), msg.len);
    while (true) {}
}

extern fn wasm_panic(ptr: u32, len: u32) u32;