from zig_wasmer.wasmer import DefaultWasmer, WasmEnvToJson
from zig_wasmer.context import Context
from zig_wasmer.compiler import ZigCompiler
import sys
import os


def main():
    zigfile = os.path.join(os.path.dirname(__file__), 'fib_wasm.zig')
    zig = ZigCompiler(debug=2)
    zig.TARGET = 'wasm32-wasi'
    wasmfile = zig.build_wasm(zigfile)

    if isinstance(wasmfile, BaseException):
        print("Compile Error:", wasmfile.cmd)
    else:
        ctx = Context()
        ctx.add_panic('wasm_panic')
        ctx.instanciate(wasmfile)

        import json
        print(json.dumps(ctx.instance.exports, indent=4, default=WasmEnvToJson))

        fib_int = int(sys.argv[1]) if len(sys.argv) > 1 else 42
        res = ctx.instance.exports.fib(fib_int)
        print("fib({})={}".format(fib_int, ctx.unsign(res, 64)))


if __name__ == '__main__':
    main()
