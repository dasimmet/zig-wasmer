
export var Member: u64 = 4;

const hello: []const u8 = "Hello, World from Zig!\n";
const member_fmt: []const u8 = "Member from Inside Zig: {d}\n";

export const formatptr = hello.ptr;
export const formatlen = hello.len;

export fn main() void {
    errdefer @panic("Hello World Failed somehow!\n");
    const std = @import("std");

    const stdout = std.io.getStdOut().writer();
    try stdout.print(hello, .{});
    try stdout.print(member_fmt, .{Member});
}