from .compiler import ZigCompiler


def modules(setup_args, zig_files: list = []):
    # if not 'package_data' in setup_args:
    #     setup_args['package_data'] = {}
    # if not '*' in setup_args['package_data']:
    #     setup_args['package_data']['*'] = []

    # setup_args['package_data']['*'].append('*.wasm')
    # setup_args['package_data']['*'].append('*.zig')

    # setup_args.update({
    #     "include_package_data": True,
    # })

    zig = ZigCompiler()
    zig.ZIG_WASMER_RECOMPILE = True
    for f in zig_files:
        print(f)
        build_zig(zig, setup_args, f)


def build_zig(zig: ZigCompiler, setup_args, f):
    if 'mode' in f:
        zig.SHORT_BUILD_ARGS['-O'] = f['mode']
    else:
        zig.SHORT_BUILD_ARGS['-O'] = 'ReleaseSafe'
    if 'target' in f:
        zig.TARGET = f['target']
    else:
        zig.TARGET = ZigCompiler.TARGET
    res = zig.build_wasm(f['src'])
    if isinstance(res, Exception):
        raise res
    print(res)
