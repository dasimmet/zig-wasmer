from .context import Context


class WasmProperty:
    def __getattr__(self, __name):
        getter = '__get_' + __name
        if getter in self.__dict__:
            return self.__dict__[getter]()
        return super().__getattr__(__name)

    def __setattr__(self, __name: str, __value) -> None:
        setter = '__set_' + __name
        if setter in self.__dict__:
            return self.__dict__[setter](__value)
        return super().__setattr__(__name, __value)

    def __add_prop__(self, name, fget=None, fset=None):
        if fget:
            self.__setattr__('__get_' + name, fget)
        if fset:
            self.__setattr__('__set_' + name, fset)


class WasmDecorator:
    @classmethod
    def Int(cls, size=32):
        return lambda ctx, f: WasmInt(ctx, f, size=size)

    @classmethod
    def ByteArray(cls, length: str):
        return lambda ctx, f: WasmByteArray(ctx, f, length=length)

    @classmethod
    def DynamicByteArray(cls, length: str):
        return lambda ctx, f: WasmDynamicByteArray(ctx, f, length=length)


class WasmObject:
    ctx: Context = None
    exp = None

    def __init__(self, ctx: Context, exp) -> None:
        self.ctx = ctx
        self.exp = exp

    def get(self):
        raise NotImplementedError(self.get)

    def set(self, value):
        raise NotImplementedError(self.set)


class WasmInt(WasmObject):
    size = None

    def __init__(self, *args, size=32, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.size = size

    def get(self):
        return self.ctx.getInt(self.exp.value, self.size)

    def set(self, v):
        self.ctx.setInt(self.exp.value, v, self.size)


class WasmByteArray(WasmObject):
    length = None

    def __init__(self, *args, length: int, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.length = length

    def get(self):
        ptr = self.ctx.getInt(self.exp.value)
        return self.ctx.deref(ptr, self.length)

    def set(self, bs: bytes):
        if len(bs) > self.length:
            raise MemoryError(self)
        ptr = self.ctx.getInt(self.exp.value)
        self.ctx.setBytes(ptr, bs)


class WasmDynamicByteArray(WasmByteArray):
    def __init__(self, *args, length: str, **kwargs) -> None:
        WasmObject.__init__(self, *args, **kwargs)
        self._lengthptr = length

    @property
    def length(self):
        lengthptr = getattr(self.ctx.instance.exports, self._lengthptr).value
        return self.ctx.getInt(lengthptr)

    @length.setter
    def length(self, value):
        lengthptr = getattr(self.ctx.instance.exports, self._lengthptr).value
        return self.ctx.setInt(lengthptr, value)
