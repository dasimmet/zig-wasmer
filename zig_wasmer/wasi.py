
class Wasi:
    name = None
    args = []
    map_dir = {}

    def __init__(self, name: str, args: list = []) -> None:
        self.name = name
        self.args = args
