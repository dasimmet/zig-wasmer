from .wasmer import WasmPanic, ZigWasmer, DefaultWasmer
from functools import lru_cache
from .wasi import Wasi


class Context:
    '''A context to instanciate wasm from.
    The 'instance' and 'wasmer' attributes are populated by 'instanciate' is calls.
    '''
    instance = None
    wasmer = None
    environment = {}

    def __init__(self, wasmer: ZigWasmer = DefaultWasmer) -> None:
        self.wasmer = wasmer

    def deref(self, ptr: int, length: int):
        view = memoryview(self.instance.exports.memory.buffer)
        return view[ptr:ptr + length]

    def getInt(self, ptr: int, size: int = 32):
        return int.from_bytes(self.deref(ptr, int(size / 8)), "little")

    def getMem(self, ptr: int, len: int, size: int = 32):
        return self.deref(self.getInt(ptr, size=size), self.getInt(len, size=size))

    def setBytes(self, ptr: int, value: bytes):
        size = len(value)
        view = self.deref(ptr, size)
        for i in range(size):
            view[i] = value[i]

    def setInt(self, ptr: int, value: int, size: int = 32):
        v = int.to_bytes(value, size, "little")
        self.setBytes(ptr, v)

    def instanciate(self, wasmfile: str, wasi: Wasi = None, environment: dict = None):
        if environment is not None:
            self.environment = environment
        self.instance = self.wasmer.load(wasmfile, self.environment, wasi=wasi)

    def add_panic(self, name: str):
        '''adds a panic function that will read the message from memory
        '''
        from wasmer import Function, FunctionType, Type

        def panic(message_ptr, message_length):
            message = self.deref(self.unsign(message_ptr), self.unsign(message_length))
            raise WasmPanic(bytes(message).decode())

        panicfn = Function(self.wasmer.store, panic, FunctionType([Type.I32, Type.I32], [Type.I32]))
        if 'env' not in self.environment:
            self.environment['env'] = {}
        self.environment['env'][name] = panicfn

    @staticmethod
    @lru_cache(maxsize=None)
    def unsign(num: int, size: int = 32):
        if num < 0:
            return 2**(size) + num
        return num
