from .compiler import ZigCompiler
from .wasmer import DefaultWasmer
from .context import Context

from .decorators import WasmProperty, WasmDecorator


class WasmModule:
    exports = None
    compiler = None
    wasmer = None
    context = None
    decorators = WasmDecorator
    wrappers = {
    }

    @classmethod
    def initialize(cls, name=None, fname=None, args=[], instanciate=True):
        cls.exports = WasmProperty()
        if name is None:
            name = cls.__name__
        if fname is None:
            basename, fname = cls.zigfile()
        cls.fname = fname
        cls.basename = basename

        if cls.compiler is None:
            cls.compiler = ZigCompiler()
            cls.compiler.DEBUG = 2

        if cls.wasmer is None:
            cls.wasmer = DefaultWasmer

        cls.context = Context()

        if instanciate:
            mod = cls.build()
            cls.instanciate(mod, args)

    @classmethod
    def zigfile(cls):
        import inspect
        classmod = inspect.getmodule(cls)
        import os
        base = os.path.splitext(os.path.basename(classmod.__file__))[0]
        fname = os.path.join(os.path.dirname(classmod.__file__), base + '.zig')
        return base, fname

    @classmethod
    def to_dict(cls):
        cls.initialize(instanciate=False)
        return {
            'src': cls.fname,
            'target': cls.compiler.TARGET,
        }

    @classmethod
    def build(cls):
        mod = cls.compiler.build_wasm(cls.fname)
        if isinstance(mod, Exception):
            raise mod
        return mod

    @classmethod
    def instanciate(cls, mod: str, args: list = []):
        cls.context.instanciate(mod)
        cls.do_decorate()

    @classmethod
    def do_decorate(cls):
        # import inspect
        # classmod = inspect.getmodule(cls)
        for name, exp in cls.context.instance.exports:
            if name in cls.decorate:
                o = cls.decorate[name](cls.context, exp)
                cls.exports.__add_prop__(name, o.get, o.set)
            else:
                setattr(cls.exports, name, exp)

    @classmethod
    def call(cls, name, *args, **kwargs):
        for fname, exp in cls.context.instance.exports:
            if fname == name:
                return exp(*args, **kwargs)
        raise Exception('Function {} not found: {}'.format(name, cls.main_function))


class WasiModule(WasmModule):

    @classmethod
    def initialize(cls, *args, **kwargs):
        super().initialize(*args, instanciate=False, **kwargs)
        cls.compiler.TARGET = "wasm32-wasi"
        super().initialize(*args, **kwargs)

    @classmethod
    def instanciate(cls, mod: str, args: list = []):
        from .wasi import Wasi
        wasi = Wasi(cls.basename, args=args)

        cls.context.instanciate(mod, wasi=wasi)
        cls.do_decorate()
