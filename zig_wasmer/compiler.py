
class ZigCompiler:
    cmd: str = None
    ZIG_NOT_FOUND_ERROR = """\
zig compier not found in PATH. \
Either install zig manually or pass a 'download_version' enable auto install"""
    ZIG_WASMER_RECOMPILE = False
    LONG_BUILD_ARGS: list = {
        '-mcpu': 'generic',
    }
    SHORT_BUILD_ARGS: dict = {
        '-r': 'dynamic',
        '-O': 'Debug',
    }
    FLAGS: list = [
        '-dynamic',
        # '--export-table',
    ]
    TARGET: str = 'wasm32-freestanding'
    DEBUG = 0

    def __init__(self, cmd: str = None, download_version: str = None, debug: int = 0) -> None:
        import os
        if os.getenv('ZIG_WASMER_RECOMPILE', None) == '1':
            self.ZIG_WASMER_RECOMPILE = True
        if cmd is None:
            from shutil import which
            cmd = which('zig')
        if cmd is None:
            cmd = self.download(download_version)
        self.cmd = cmd
        self.DEBUG = debug
        self.LONG_BUILD_ARGS = self.__class__.LONG_BUILD_ARGS.copy()
        self.SHORT_BUILD_ARGS = self.__class__.SHORT_BUILD_ARGS.copy()
        self.FLAGS = self.__class__.FLAGS.copy()

    def download(self, download_version: str):
        import logging
        import os
        if download_version is None:
            if self.ZIG_WASMER_RECOMPILE:
                raise Exception(self.ZIG_NOT_FOUND_ERROR)
            else:
                logging.warn(self.ZIG_NOT_FOUND_ERROR)
                return
        tmpdir = None
        for d in [os.getenv('XDG_CACHE_HOME', None), '/var/tmp', '~/.cache', '/tmp']:
            if d is None:
                continue
            d = os.path.expanduser(d)
            tmpdir = os.path.join(d, 'zig-wasmer')
            if dir_is_creatable(tmpdir):
                break
        if tmpdir is None:
            raise Exception('No creatable Temporary directory found')
        print(tmpdir)
        return os.path.join(tmpdir, 'zig')

    def args(self):
        args = []
        for k, v in self.LONG_BUILD_ARGS.items():
            args.append(k)
            args.append(v)
        for k, v in self.SHORT_BUILD_ARGS.items():
            args.append(k + v)
        args.extend(['-target', self.TARGET])
        return args + self.FLAGS

    def build_wasm(self, input: str, output: str = None, args=[]):
        import os
        if output is None:
            output = os.path.splitext(input)[0] + '.wasm'

        if os.path.isfile(output) and not self.ZIG_WASMER_RECOMPILE:
            return output

        BUILD_CMD = [self.cmd, 'build-lib', input, '-femit-bin=' + output] + self.args() + args
        from subprocess import check_call
        if self.DEBUG > 0:
            print(BUILD_CMD)

        try:
            check_call(BUILD_CMD)
        except:
            return CompilerException('Zig Build Lib Failed:', BUILD_CMD)
        if self.DEBUG > 1:
            watfile = os.path.splitext(input)[0] + '.wat'
            from wasmer import wasm2wat
            with open(output, 'rb') as wasmfd:
                with open(watfile, 'w') as watfd:
                    watfd.write(wasm2wat(wasmfd.read()))
        return output


class CompilerException(Exception):
    def __init__(self, message, cmd):
        super().__init__(self, message)

        self.cmd = cmd


def dir_is_creatable(d):
    import os
    head, tail = (head, None)
    while head not in ['/', '']:
        exists = os.path.exists(head)
        isdir = os.path.isdir(head)
        if exists and not isdir:
            return False
        writable = os.access(head, os.W_OK)
        if isdir and writable:
            return True
        head, tail = os.path.split(head)
    return False
