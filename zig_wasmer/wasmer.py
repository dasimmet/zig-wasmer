
import wasmer
from .wasi import Wasi


class ZigWasmer:
    engine = None
    store = None
    moduleCache = {}

    def __init__(self, compiler=None):

        if compiler is None:
            from wasmer_compiler_singlepass import Compiler
            compiler = Compiler

        if self.engine is None:
            self.engine = wasmer.engine.Universal(compiler)
        if self.store is None:
            self.store = wasmer.Store(self.engine)

    def load(self, wasmfile, imports={}, cache=True, wasi: Wasi = None):
        import os
        fh = hash(os.path.abspath(wasmfile))
        if cache and fh in self.moduleCache:
            module = self.moduleCache[fh]
        else:
            with open(wasmfile, 'rb') as fd:
                f = fd.read()
            module = wasmer.Module(self.store, f)
            if cache:
                self.moduleCache[fh] = module

        if wasi is not None:
            wasi_version = wasmer.wasi.get_version(module, strict=True) or 1
            wasi_env = wasmer.wasi.StateBuilder(wasi.name)
            for k, v in wasi.map_dir.items():
                wasi_env = wasi_env.map_directory(k, v)
            for arg in wasi.args:
                wasi_env = wasi_env.argument(arg)
            wasi_env = wasi_env.finalize()

            import_object = wasi_env.generate_import_object(self.store, wasi_version)
            i = import_object.to_dict()
            i.update(imports)
            imports = i

        return wasmer.Instance(module, imports)


def WasmEnvToJson(o):
    from wasmer import Function, Type, Exports, Global, Table, Memory, MemoryType
    common = {
        'type': o.__class__.__name__,
    }
    if isinstance(o, Function):
        common['params'] = [Type(i).name for i in o.type.params]
        common['results'] = [Type(i).name for i in o.type.results]
        return common

    if isinstance(o, Exports):
        common['exports'] = {n: e for n, e in o}
        return common

    if isinstance(o, Global):
        common['GlobalType'] = Type(o.type.type).name
        common['value'] = o.value
        common['mutable'] = o.mutable
        return common

    if isinstance(o, Table):
        common['TableType'] = Type(o.type.type).name,
        common['size'] = o.size
        return common

    if isinstance(o, Memory):
        common['size'] = o.size
        common['type'] = {
            'minimum': o.type.minimum,
            'maximum': o.type.maximum,
            'shared': o.type.shared,
        }
        return common
    return str(o)


class WasmException(Exception):
    pass


class WasmPanic(WasmException):
    pass


DefaultWasmer = ZigWasmer()
