# Zig Wasmer Python Plugin

This is to explore python integration with zig and wasmer.
Basically the idea is to have zig code compiled to wasm and run by wasmer instead of native cpython extensions.
The result would be a single binary distribution (`wheel`) running wherever `wasmer` supports it.

When a library using this is imported, it should use a hook to:

- check if it has the compiled wasm for a zig extension.
- try to compile it on the fly using `zig build-lib` if not.
- Always recompile if ZIG_WASMER_RECOMPILE=1 is set as an environment variable
- Load the wasm into wasmer and use the wasm context.



## Examples

These examples use `*.zig` files with equal basename.

[zig_wasmer/examples/fib_wasm.py](zig_wasmer/examples/fib_wasm.py)
```console
> python -m zig_wasmer.examples.fib_wasm 93
fib(93)=12200160415121876738
```

The command will compile a example fibonacci u64 implementation to wasm.
Then, load it into wasmer and calculate fib(93), which is the largest number to still fit into u64.  
It also prints some debug information about the wasm module.

```console
> python -m zig_wasmer.examples.fib_wasm 94
zig_wasmer.wasmer.WasmPanic: Fib I64 Overflow
```

Now, fib(94) will throw a Zig Panic and pass the message to a Python exception using an imported panic function.  

[zig_wasmer/examples/fib_wasi.py](zig_wasmer/examples/fib_wasi.py)  
This is the same example as above, but using wasi for the exception handling.

[zig_wasmer/examples/module.py](zig_wasmer/examples/module.py)
```console
> python -m zig_wasmer.examples.module
Reading Wasm Member from Python: 4
Exported String in Wasm Memory:  b'Hello, World from Zig!\n'
Setting Wasm Member to: 4242
Running Main function:
----------------------
Hello, World from Zig!
Member from Inside Zig: 4242
```

This module subclasses zig_wasmer.module.WasiModule,
which is used to compile and load all exports
of module.zig at import time.
In the example the exported variable is altered and printed from zig.


## Precompiled .wasm
During the python package build stage, the wasm binaries can be precompiled and included.

### build_ext.py

This feels a bit hacky for now, but it will compile the wasm modules (by default in ReleaseSafe mode) when `python -m build . --wheel`
is run on the project.
```python
def build(setup_kwargs):
    from zig_wasmer import build
    from zig_wasmer.examples import module 

    build.modules(setup_kwargs, zig_files=[
        {
            'src': 'zig_wasmer/examples/fib_wasi.zig',
            'target': 'wasm32-wasi',
        },
        {
            'src': 'zig_wasmer/examples/module.zig',
            'target': 'wasm32-wasi',
        },
        module.Example.to_dict(),
    ])
```

### pyproject.toml

The built .wasm files have to be included in the binary wheel distribution and the build script referenced.  
At Runtime we only depend on `wasmer` and a compiler.
```toml
[tool.poetry]
include = [
    { path = "**/*.wasm", format = ["wheel"] },
    { path = "**/*.zig", format = ["sdist","wheel"] }
]
build = "build_ext.py"

[tool.poetry.dependencies]
wasmer = "1.1.0"
wasmer_compiler_singlepass = "1.1.0"
```

## TODO

- define a cleaner example interface